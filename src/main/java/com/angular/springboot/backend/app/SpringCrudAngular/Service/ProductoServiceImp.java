package com.angular.springboot.backend.app.SpringCrudAngular.Service;

import java.util.List;

import org.springframework.aop.AopInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.angular.springboot.backend.app.SpringCrudAngular.Dao.IProductoDao;
import com.angular.springboot.backend.app.SpringCrudAngular.Models.Entity.Producto;

@Service
public class ProductoServiceImp implements IProducto{
	
	@Autowired
	IProductoDao productoDao;

	@Transactional(readOnly = true)	
	@Override
	public List<Producto> getProducts() {
		
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	public void saveProduct(Producto product) {
		 productoDao.save(product);		
	}

	@Transactional(readOnly = true)
	@Override
	public Producto findById(Long id) {
		return productoDao.findById(id).orElse(null);
	}

	@Transactional(readOnly = true)
	@Override
	public Producto findByNombre(String nombre) {
		return productoDao.findByIdNombre(nombre);
	}
	
	@Override
	public void deleteById(Long id) {
		productoDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)	
	public long getTotal() {
		// TODO Auto-generated method stub
		return productoDao.count();
	}
	
	@Override
	public int countByNombre(String nombre) {
		
		int registros = 0;
		
		try {
			 registros = productoDao.countByNombre(nombre);
		} catch (AopInvocationException e) {
			
		}
		
		return registros;
	}
	


}
