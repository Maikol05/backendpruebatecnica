package com.angular.springboot.backend.app.SpringCrudAngular.Controllers;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.angular.springboot.backend.app.SpringCrudAngular.Models.Entity.Producto;
import com.angular.springboot.backend.app.SpringCrudAngular.Service.IProducto;



@RestController
@RequestMapping("/product")
@CrossOrigin(origins = {"*"})
public class ProductoController {

	@Autowired
	private IProducto productService;
	
	//ruta para traer todos los registros de la base de datos
	@GetMapping("/listar")
	public List<Producto> index(){
		List<Producto> lista = productService.getProducts();
		return lista;
	}
	
	@PostMapping("/guardar")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@RequestBody Producto product) {
		
		Producto producto = null;
		Map<String,Object> response = new HashMap<>();
		System.out.println(product.getFecha());
			
			try {
				productService.saveProduct(product);
				
			}catch (DataAccessException e) {
				
				response.put("mensaje","Error, el registro ya existe");
				response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				
			}	
			
			response.put("mensaje", "El producto ha sido creado con exito");
			response.put("Producto", producto);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@ResponseStatus(HttpStatus.CREATED)//ruta para editar un registro de la base de datos
	@PutMapping("/editar")
	//ruta que traslada un registro de la base de datos a la vista para modificarlo
	public ResponseEntity<?> update(@RequestBody Producto product) throws ParseException {
		
		Producto productoUpdated = null;
		Map<String,Object> response = new HashMap<>();


			 Producto productoBaseDatos = productService.findByNombre(product.getNombre());
			 
			 if(productoBaseDatos == null) {
				 response.put("error", "Error, no se pudo editar el producto  ".concat(product.getNombre().concat(". No existe en la base de datos")));
				 response.put("Producto", productoUpdated);
				 return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			 }
		
		int total, tope;
		
			String fecha1 = product.getClase().substring(0,7);
			String fecha2 = productoBaseDatos.getFecha().toString().substring(0, 7); 
			
			if(!fecha1.equals(fecha2)) {
				
				total = productoBaseDatos.getRegistrados()+product.getRegistrados();
				tope = productoBaseDatos.getTope();
				
				if(total > tope) {
					
					response.put("mensaje", "la cantidad no se registro debido a que superaria el tope "+productoBaseDatos.getTope());
					response.put("Producto", productoUpdated);
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
				}
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(product.getClase());
				productoBaseDatos.setRegistrados(total);
				productoBaseDatos.setFecha(date);
				productService.saveProduct(productoBaseDatos);
				
			}else {
				response.put("mensaje", "No se ha registrado porque ya se hizo este mes");
				response.put("Producto", productoUpdated);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
			}
			
		
		response.put("mensaje", "producto "+product.getNombre()+" con cantidad actualizada a "+total+" y tope "+tope);
		response.put("Producto", productoUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
}
