package com.angular.springboot.backend.app.SpringCrudAngular.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.angular.springboot.backend.app.SpringCrudAngular.Models.Entity.Producto;

public interface IProductoDao extends JpaRepository<Producto, Long>{
	
	@Query("SELECT l.registrados from Producto as l where l.nombre=:nombre")
	public int countByNombre(@Param("nombre") String nombre);

	@Query(value ="SELECT * from Producto as l where l.nombre=:nombre", nativeQuery = true)
	public Producto findByIdNombre(@Param("nombre") String nombre);
	
}
