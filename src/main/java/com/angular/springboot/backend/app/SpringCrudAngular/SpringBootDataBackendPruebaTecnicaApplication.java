package com.angular.springboot.backend.app.SpringCrudAngular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataBackendPruebaTecnicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataBackendPruebaTecnicaApplication.class, args);
	}

}
