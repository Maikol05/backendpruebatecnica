package com.angular.springboot.backend.app.SpringCrudAngular.Models.Entity;

import java.io.Serializable;
import java.time.Year;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.validation.constraints.NotBlank;

import com.sun.istack.NotNull;

@Entity
@Table(name="producto")
public class Producto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "El campo no debe estar vacido")
	@Column(name="nombre", length = 50, unique = true)
	private String nombre;
	
	@Column(name="tipo_producto", length = 50)
	private String clase;
	
    private int registrados;
	
	@NotNull
	private int tope;
	
	@NotNull
	private Date fecha;
	

	public int getRegistrados() {
		return registrados;
	}

	public void setRegistrados(int registrados) {
		this.registrados = registrados;
	}

	public int getTope() {
		return tope;
	}

	public void setTope(int tope) {
		this.tope = tope;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public Long getId() {
		return id;
	}
	
}
