package com.angular.springboot.backend.app.SpringCrudAngular.Service;

import java.util.List;

import com.angular.springboot.backend.app.SpringCrudAngular.Models.Entity.Producto;

public interface IProducto {

	public List<Producto> getProducts();
	
	public void saveProduct(Producto product);
	
	public int countByNombre(String nombre);
	
	public Producto findById(Long id);
	
	public Producto findByNombre(String nombre);
	
	public void deleteById(Long id);
	
	public long getTotal();
	
}
